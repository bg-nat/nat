Import-Module GoogleCloud
Import-Module -Name 'C:\Program Files\Microsoft\Exchange\Web Services\2.2\Microsoft.Exchange.WebServices.dll'
$paths =  'gs:\nat-new-db' , 'gs:\nat-old-db' , 'gs:\nat-new-db-archive'
$subject = "Backup report " + (Get-Date -Format 'dd-MM-yyyy').ToString()
$recipient = 'm.doronin@bg-nat.com'
$style = '<style> head{font-family: Arial; font-size: 10pt;}
                       h1 {
                           font-family: Calibri;
                           display: block;
                           font-size: 24pt;
                           margin-top: 0.67em;
                           margin-bottom: 0.67em;
                           margin-left: 0;
                           margin-right: 0;
                           font-weight: bold;
                       }
                       h4 {
                           font-family: Calibri;
                           display: block;
                           font-size: 10pt;
                           margin-left: 0;
                           margin-right: 0;
                           margin-top: 0;
                           margin-bottom: 0;
                           font-weight: normal;
                           line-height: 1.5;
                       }'
   $style = $style + 'table{border: 1px solid black; border-collapse: collapse;}'
   $style = $style + 'th{border: 1px solid black; background: #dddddd; padding: 5px; }'
   $style = $style + 'td{border: 1px solid black; padding: 5px; }'
   $style = $style + '</style>'
[string]$htmlOut = $style
foreach ($item in $paths)
{
   $htmlOut += Get-ChildItem -Path $item -Recurse  |  Where-Object { $_.TimeCreated -gt  (Get-Date).AddDays(-1) -and $_.Name -like "*.bak" }  |Select-Object -Property Name , Size | ConvertTo-Html -Head ''
}
$htmlOut


try{
    $exchService = New-Object -TypeName Microsoft.Exchange.WebServices.Data.ExchangeService

    $PlainPassword = "Qwerty1"
    $SecurePassword = $PlainPassword | ConvertTo-SecureString -AsPlainText -Force
    $UserName = "1Creport@bg-nat.com"
    $Credentials = New-Object System.Management.Automation.PSCredential -ArgumentList $UserName, $SecurePassword

    $exchService.Credentials = New-Object -TypeName Microsoft.Exchange.WebServices.Data.WebCredentials -ArgumentList $Credentials.UserName, $Credentials.GetNetworkCredential().Password
    $exchService.AutodiscoverUrl($Credentials.UserName, {$true})
    $exchService.Url = 'https://outlook.office365.com/EWS/Exchange.asmx'
    $eMail = New-Object -TypeName Microsoft.Exchange.WebServices.Data.EmailMessage -ArgumentList $exchService
    $eMail.Subject = $subject
    $eMail.Body = $htmlOut.ToString()
    $eMail.ToRecipients.Add($recipient) | Out-Null
    $eMail.SendAndSaveCopy()
}
catch
{
    $ErrorMessage = $_.Exception.Message
    $ErrorMessage
    $FailedItem = $_.Exception.ItemName
    $FailedItem
    $ErrorMessage | Out-File -FilePath C:\scripts\log\mailSend.log -Append -Force
    $FailedItem | Out-File -FilePath C:\scripts\log\mailSend.log -Append -Force
}
