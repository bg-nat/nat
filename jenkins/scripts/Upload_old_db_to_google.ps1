Param(
    [string]$ListDB,
    [string]$TypeBackup,
    [bool]$RevertDB = $false
)
Write-Host "$ListDB $TypeBackup $RevertDB"

#ListDB ARM-AM|erp-nat-200719|SecondSales
#$TypeBackup *.bak *.bak
$TypeBackup = '*.' + $TypeBackup
Import-Module GoogleCloud

Write-Host "Get backup files"
if($RevertDB){
    [string]$googleFolder = 'nat-old-db'
    $filesFull = Get-ChildItem -Path \\db01-nat\b$\backup -Recurse -Include $TypeBackup | Where-Object {$_.LastWriteTime -gt  [datetime]::Today -and $_.PSParentPath -notmatch $ListDB }
}
else{
    [string]$googleFolder = 'nat-new-db'
    if ((Get-Date).AddDays(1).Day -eq 1 -or (Get-Date).AddDays(1).Day -eq 2)
    {
    [string]$googleFolder = 'nat-new-db-archive'
    }
    $filesFull = Get-ChildItem -Path \\db01-nat\b$\backup -Recurse -Include $TypeBackup | Where-Object {$_.LastWriteTime -gt  [datetime]::Today -and $_.PSParentPath -match $ListDB }
}

cd gs:\$googleFolder
$curentPath = (Get-Location).Path
Write-Host $curentPath
foreach ($file in $filesFull)
{
    $folder = $file.Directory.Name
    if(Test-Path -Path gs:\$googleFolder\$folder){
        Write-Host "folder exists gs:\$googleFolder\$folder" -ForegroundColor Green
    }
    else{
        Write-Host "folder not exists gs:\$googleFolder\$folder" -ForegroundColor Yellow
        Write-Host "Creating... " -ForegroundColor Yellow
        mkdir $folder
    }

}
cd c:\
foreach ($file in $filesFull)
{
    $folder = $file.Directory.Name
    $fileName = $file.Name
    if(Test-Path -Path gs:\$googleFolder\$folder\$fileName){
        Write-Host "file exists gs:\$googleFolder\$folder\$fileName" -ForegroundColor Green
    }
    else{
        Write-Host "file not exists gs:\$googleFolder\$folder\$fileName" -ForegroundColor Yellow
        Write-Host "Uploading...." -ForegroundColor Yellow
        $ObjectName = $file.Directory.Name + "/" + $file.Name
        Write-GcsObject -Bucket $googleFolder -ObjectName $ObjectName -File $file.FullName -Force
    }
}
